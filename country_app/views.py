from django.shortcuts import render
from .models import Countries, States, Cities
import django_filters.rest_framework
from rest_framework import generics
from .serializers import CountriesSerializer, StateSerializer, CitiesSerializer


class CountriesAPIView(generics.ListCreateAPIView):
    def get_queryset(self):
        return Countries.objects.all()

    def perform_create(self, serializer):
        serializer.save()

    serializer_class = CountriesSerializer
    


class CountriesDetailsView(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return Countries.objects.all()

    serializer_class = CountriesSerializer


class StatesAPIView(generics.ListCreateAPIView):
    def get_queryset(self):
        return States.objects.all()

    def perform_create(self, serializer):
        serializer.save()

    serializer_class = StateSerializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['Country']


class StatesDetailsView(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return States.objects.all()

    serializer_class = StateSerializer


class CitiesAPIView(generics.ListCreateAPIView):
    def get_queryset(self):
        return Cities.objects.all()

    def perform_create(self, serializer):
        serializer.save()

    serializer_class = CitiesSerializer
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]
    filterset_fields = ['State']

class CitiesDetailsView(generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return Cities.objects.all()

    serializer_class = CitiesSerializer
