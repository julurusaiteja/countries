"""country_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from country_app import views


urlpatterns = [
    #country
    path('create_country/',views.CountriesAPIView.as_view(),name='createcountry'),
    path('updatedelete_country/<int:pk>',views.CountriesDetailsView.as_view(),name='updatedeletecountry'),
#state
    path('create_state/', views.StatesAPIView.as_view(), name='createstate'),
    path('updatedelete_state/<int:pk>', views.StatesDetailsView.as_view(), name='updatedeletestate'),
#cities
    path('create_city/', views.CitiesAPIView.as_view(), name='createcity'),
    path('updatedelete_city/<int:pk>', views.CitiesDetailsView.as_view(), name='updatedeletecity'),
]
