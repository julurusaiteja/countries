from .models import Countries, Cities, States
from rest_framework import serializers


class CountriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Countries
        fields = '__all__'
        


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = States
        fields = '__all__'
        
    def to_representation(self, instance):
        rep = super(StateSerializer, self).to_representation(instance)
        rep['Country'] = instance.Country.Name
        return rep

class CitiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cities
        fields = '__all__'

    
    def to_representation(self, instance):
       rep = super(CitiesSerializer, self).to_representation(instance)
       rep['State'] = instance.State.Name
       return rep
