from django.db import models


# Create your models here.
class Countries(models.Model):
    Name = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.id}-{self.Name}'


class States(models.Model):
    Country = models.ForeignKey(Countries, on_delete=models.CASCADE)
    Name = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.id}-{self.Country}-{self.Name}'


class Cities(models.Model):
    State = models.ForeignKey(States, on_delete=models.CASCADE)
    Name = models.CharField(max_length=200)

    def __str__(self):
        return f'{self.id}-{self.State}-{self.Name}'
