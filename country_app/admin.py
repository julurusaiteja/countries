from django.contrib import admin
from  .models import Countries,States,Cities
# Register your models here.
class CountriesAdmin(admin.ModelAdmin):
    list_display = ['id','Name']
admin.site.register(Countries,CountriesAdmin)
class StatesAdmin(admin.ModelAdmin):
    list_display = ['id','Country','Name']
admin.site.register(States,StatesAdmin)
class CitiesAdmin(admin.ModelAdmin):
    list_display = ['id','State','Name']
admin.site.register(Cities,CitiesAdmin)